jQuery(document).ready(function() {
    var $wrapper = $('.js-genus-scientist-wrapper');
    $wrapper.on('click', '.js-genus-scientist-add', function(e) {
        e.preventDefault();
        var prototype = $wrapper.data('prototype');
        var index = $wrapper.data('index');
        var newForm = prototype.replace(/__name__/g, index);
        $wrapper.data('index', index + 1);
        $(this).before(newForm);
    });

    var $wrapper2 = $('.js-makings-wrapper');
    $wrapper2.on('click', '.js-makings-add', function(e) {
        e.preventDefault();
        var prototype = $wrapper2.data('prototype');
        var index2 = $wrapper2.data('index');
        var newForm2 = prototype.replace(/__name__/g, index2);
        $wrapper2.data('index', index2 + 1);
        $(this).before(newForm2);
    });
});