<?php

namespace App\Repository;

use App\Entity\Making;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Making|null find($id, $lockMode = null, $lockVersion = null)
 * @method Making|null findOneBy(array $criteria, array $orderBy = null)
 * @method Making[]    findAll()
 * @method Making[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MakingRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Making::class);
    }

    // /**
    //  * @return Making[] Returns an array of Making objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('m.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Making
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
