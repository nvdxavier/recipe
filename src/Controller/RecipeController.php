<?php

namespace App\Controller;

use App\Entity\Ingredient;
use App\Entity\Making;
use App\Entity\Recipe;
use App\Form\IngredientsType;
use App\Form\MakingType;
use App\Form\RecipeType;
use App\Service\FileUploader;
use Doctrine\ORM\EntityNotFoundException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;


class RecipeController extends AbstractController
{
    /**
     *   @Route("/show/recipe", name="show_all_recipe")
     * @return Response
     */
    public function recipesShowAction()
    {
        $em = $this->getDoctrine()->getManager();
        $recipes = $em->getRepository(Recipe::class)->findAll();

        return $this->render('recipe/show.html.twig', [
            'recipes' => $recipes
        ]);
    }

    /**
     * @Route("/",name="create_recipe")
     * @param Request $request
     * @param FileUploader $fileUploader
     * @return Response
     * @throws \Exception
     */
    public function createRecipeAction(Request $request, FileUploader $fileUploader)
    {
        $newrecipe = new Recipe();

        $formRecipe = $this->createForm(RecipeType::class, $newrecipe,[
//            'action' => $this->generateUrl('api_post_new_recipe'),
            'attr' => ['id' => 'formrecipe'],
        ]);
        $formRecipe->handleRequest($request);

        if($formRecipe->isSubmitted()){
            $reciperequest = $request->request->get('recipe');
            $newrecipe->setTitle($reciperequest['title']);
            $newrecipe->setCoockingTime(new \DateTime($reciperequest['coocking_time']));
            $newrecipe->setDifficulty($reciperequest['difficulty']);
            $newrecipe->setPreparationTime(new \DateTime($reciperequest['preparation_time']));
            $newrecipe->setPersonalNote($reciperequest['personal_note']);

            if(isset($reciperequest['ingredients'])){
                foreach($reciperequest['ingredients'] as& $value){
                    $ingredient = new Ingredient();
                    $ingredient->setTitle($value['title']);
                    $ingredient->setQuantity($value['quantity']);
                    $ingredient->setRecipe($newrecipe);
                    $newrecipe->getIngredients()->add($ingredient);
                }
            }

            $recipeimage = $request->files->get('recipe');

            if (null != $recipeimage['recipeimage']) {

                /** @var UploadedFile $brochureFile */
                $brochureFile = $recipeimage['recipeimage'];
                if ($brochureFile) {
                    $brochureFileName = $fileUploader->upload($brochureFile);
                    $newrecipe->setRecipeImage($brochureFileName);
                }
            }

            if(isset($reciperequest['makings'])){

                foreach($reciperequest['makings'] as &$value){
                    $makings = new Making();
                    $makings->setStepmaking($value['stepmaking']);
                    $makings->setOrdermaking($value['ordermaking']);
                    $makings->setRecipe($newrecipe);
                    $newrecipe->getMakings()->add($makings);
                }
            }
            $em = $this->getDoctrine()->getManager();
            $em->persist($newrecipe);
            $em->flush();

            return $this->redirectToRoute('show_all_recipe');
        }

        return $this->render('recipe/create.html.twig', [
            'controller_name' => 'HomeController',
            'formrecipe' => $formRecipe->createView()
        ]);
    }

    /**
     * @Route("/new/recipe", name="new_recipe")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function recipeCreationAction()
    {
        $newrecipe = new Recipe();
        $formRecipe = $this->createForm(RecipeType::class, $newrecipe,[
//            'action' => $this->generateUrl('api_post_new_recipe'),
            'attr' => ['id' => 'formrecipe'],
        ]);
        $formRecipe->handleRequest($newrecipe);



        return $this->render('recipe/index.html.twig', [
            'controller_name' => 'HomeController',
            'formrecipe' => $formRecipe->createView()
        ]);
    }

    /**
     * @Route("/update/recipe/{id}", name="update_recipe")
     * @param Request $request
     * @param int $id
     * @param FileUploader $fileUploader
     * @return Response
     * @throws EntityNotFoundException
     */
    public function recipeUpdateAction(Request $request, int $id, FileUploader $fileUploader)
    {
        $em = $this->getDoctrine()->getManager();
        $reciperepository = $em->getRepository(Recipe::class)->find($id);
        if (!$reciperepository) {
            throw new EntityNotFoundException('This Recipe doen\'t exist');
        }

        $formUpdateRecipe = $this->createForm(RecipeType::class, $reciperepository, [
            'attr' => ['id' => 'formUpdateRecipe'],
            'method' => 'post'
        ]);
        $formUpdateRecipe->handleRequest($request);


        $newingredient = new Ingredient();
        $formnewingredient = $this->createForm(IngredientsType::class, $newingredient, [
//            'action' => $this->generateUrl('api_post_new_recipe'),
            'attr' => ['id' => 'formneingredient'],
            'method' => 'post'
        ])->add('submit', SubmitType::class, array('label' => 'create this ingredient ?', 'attr' => array('class' => 'btn btn-large btn-primary')));
        $formnewingredient->handleRequest($request);


        $newmaking = new Making();
        $formnewmakings = $this->createForm(MakingType::class, $newmaking,[
        'attr' => ['id' => 'formnemaking'],
            'method' => 'post'
        ])->add('submit', SubmitType::class, array('label' => 'Add a new step of preparation', 'attr' => array('class' => 'btn btn-large btn-primary')));
        $formnewmakings->handleRequest($request);
        $request->request->add();
        if ($formUpdateRecipe->isSubmitted()) {
            $recipeimage = $request->files->get('recipe');
            if (null != $recipeimage['recipeimage']) {

                /** @var UploadedFile $brochureFile */
                $brochureFile = $formUpdateRecipe['recipeimage']->getData();
                if ($brochureFile) {
                    $brochureFileName = $fileUploader->upload($brochureFile);
                    $reciperepository->setRecipeImage($brochureFileName);
                }
            }

            foreach($reciperepository->getIngredients() as& $value) {
                if(is_null($value->getRecipe())) {
                    $value->setRecipe($reciperepository);
                }
            }

            foreach($reciperepository->getMakings() as& $value)
            {
                if(is_null($value->getRecipe())) {
                    $value->setRecipe($reciperepository);
                }
            }
            $em->persist($reciperepository);
            $em->flush();
            return $this->redirectToRoute('update_recipe',['id' => $id]);
        }


        if ($request->getMethod() == 'POST') {

            $data_ingredients = $request->request->get('ingredients');
            if(null != $data_ingredients){
                $newingredient->setTitle($data_ingredients['title']);
                $newingredient->setQuantity($data_ingredients['quantity']);
                $reciperepository->getIngredients()->add($newingredient);
                $newingredient->setRecipe($reciperepository);
                $em->persist($newingredient);
                $em->flush();
                return new JsonResponse(true);
            }

            $data_makings= $request->request->get('making');
            if(null != $data_makings){
                $newmaking->setStepmaking($data_makings['stepmaking']);
                $newmaking->setOrdermaking($data_makings['ordermaking']);
                $reciperepository->getMakings()->add($newmaking);
                $newmaking->setRecipe($reciperepository);
                $em->persist($newmaking);
                $em->flush();
                return $this->redirectToRoute('update_recipe',['id' => $reciperepository->getId()]);

            }

        }

        return $this->render('recipe/edit.html.twig', ['formUpdateRecipe' => $formUpdateRecipe->createView(),
                                                             'newformingredient' => $formnewingredient->createView(),
                                                             'newformmakings' => $formnewmakings->createView(),
                                                             'id_repice' => $reciperepository->getId()
                                                            ]);
    }

    /**
     * @Route("/remove/element/{id}/{type}", name="remove_this_element")
     * @param string $type
     * @param int $id
     * @return JsonResponse
     */
    public function deleteThisElementAction(string $type,int $id)
    {
        $em = $this->getDoctrine()->getManager();
        if($type === 'ingredient'){
            $element = $em->getRepository(Ingredient::class)->find($id);
            $em->remove($element);
        }
        if($type === 'making'){
            $element = $em->getRepository(Making::class)->find($id);
            $em->remove($element);
        }

        $em->flush();
        return new JsonResponse($type);
    }

    /**
     * @Route("/add/element/{id}", name="add_new_element")
     * @param int $id
     * @param Request $request
     * @return JsonResponse
     */
    public function addthisElementAction(int $id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $newIngredient = new Ingredient();
        $ingredients = $request->request->get('ingredients');
        $recipe = $em->getRepository(Recipe::class)->find($id);
        $newIngredient->setTitle($ingredients['title']);
        $newIngredient->setQuantity($ingredients['quantity']);
        $newIngredient->setRecipe($recipe);
            $em->persist($newIngredient);
            $em->flush();

            return new JsonResponse(true);
    }


    /**
     * @Route("/add/stepmaking/{id}", name="add_new_stepmaking")
     * @param int $id
     * @param Request $request
     * @return JsonResponse
     */
    public function addStepMakingAction(int $id, Request $request)
    {

        $em = $this->getDoctrine()->getManager();
        $newmaking = new Making();
        $data_makings = $request->request->get('making');
        $recipe = $em->getRepository(Recipe::class)->find($id);
        $newmaking->setStepmaking($data_makings['stepmaking']);
        $newmaking->setOrdermaking($data_makings['ordermaking']);
        $newmaking->setRecipe($recipe);
        $em->persist($newmaking);
        $em->flush();

        return new JsonResponse(true);
    }

    /**
     * @Route("/delete/repice/{id}",  requirements={"id": "\d+"}, name="remove_this_repice")
     * @param int $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function recipeSuppressionAction(int $id)
    {
        $em = $this->getDoctrine()->getManager();
        $recipe = $em->getRepository(Recipe::class)->find($id);
        $em->remove($recipe);
        $em->flush();

        return $this->redirectToRoute('show_all_recipe');
    }
}
