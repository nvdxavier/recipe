<?php

namespace App\Entity;

use App\Repository\RecipeRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=RecipeRepository::class)
 */
class Recipe
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups("post:read")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups("post:read")
     */
    private $title;

    /**
     * @ORM\Column(type="time", nullable=true)
     */
    private $coocking_time;

    /**
     * @ORM\Column(type="time", nullable=true)
     */
    private $preparation_time;


    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $personal_note;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    /**
     * @Assert\File(
     *     maxSize = "3M",
     *     mimeTypes = {
     *          "image/jpeg",
     *          "image/jpg",
     *          "image/gif",
     *          "application/pdf",
     *          "application/x-pdf"},
     *     mimeTypesMessage = "Veuillez ajouter un type de fichier valide"
     * )
     * @ORM\Column(type="string", nullable=true)
     */
    private $recipeimage;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $difficulty;

    /**
     * @ORM\OneToMany(targetEntity=UserComment::class, mappedBy="recipe",orphanRemoval=true,cascade={"remove", "persist"})
     */
    private $usercomments;

    /**
     * @ORM\OneToMany(targetEntity=Ingredient::class, mappedBy="recipe",orphanRemoval=true,cascade={"remove", "persist"})
     */
    private $ingredients;

    /**
     * @ORM\OneToMany(targetEntity=Making::class, mappedBy="recipe",orphanRemoval=true,cascade={"remove", "persist"})
     */
    private $makings;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $created_at;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updated_at;


    const DIFFICULTY_EASY_PEASY = 1;
    const DIFFICULTY_EASY = 2;
    const DIFFICULTY_MEDIUM = 3;
    const DIFFICULTY_PRETTY_HARD = 4;
    const DIFFICULTY_COMPLEX= 5;

    public function __construct()
    {
        $this->usercomments = new ArrayCollection();
        $this->ingredients = new ArrayCollection();
        $this->makings = new ArrayCollection();
    }



    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(?string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getCoockingTime(): ?\DateTimeInterface
    {
        return $this->coocking_time;
    }

    public function setCoockingTime(?\DateTimeInterface $coocking_time): self
    {
        $this->coocking_time = $coocking_time;

        return $this;
    }

    public function getPreparationTime(): ?\DateTimeInterface
    {
        return $this->preparation_time;
    }

    public function setPreparationTime(?\DateTimeInterface $preparation_time): self
    {
        $this->preparation_time = $preparation_time;

        return $this;
    }


    public function getPersonalNote(): ?string
    {
        return $this->personal_note;
    }

    public function setPersonalNote(?string $personal_note): self
    {
        $this->personal_note = $personal_note;

        return $this;
    }

    public function getRecipeImage(): ?string
    {
        return $this->recipeimage;
    }

    public function setRecipeImage(?string $recipeimage): self
    {
        $this->recipeimage = $recipeimage;

        return $this;
    }

    public function getDifficulty(): ?int
    {
        return $this->difficulty;
    }

    public function setDifficulty(?int $difficulty): self
    {
        $this->difficulty = $difficulty;

        return $this;
    }

    /**
     * @return Collection|UserComment[]
     */
    public function getUsercomments(): Collection
    {
        return $this->usercomments;
    }

    public function addUsercomment(UserComment $usercomment): self
    {
        if (!$this->usercomments->contains($usercomment)) {
            $this->usercomments[] = $usercomment;
            $usercomment->setRecipe($this);
        }

        return $this;
    }

    public function removeUsercomment(UserComment $usercomment): self
    {
        if ($this->usercomments->contains($usercomment)) {
            $this->usercomments->removeElement($usercomment);
            // set the owning side to null (unless already changed)
            if ($usercomment->getRecipe() === $this) {
                $usercomment->setRecipe(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Ingredient[]
     */
    public function getIngredients(): Collection
    {
        return $this->ingredients;
    }

    public function addIngredient(Ingredient $ingredient): self
    {
        if (!$this->ingredients->contains($ingredient)) {
            $this->ingredients[] = $ingredient;
            $ingredient->setRecipe($this);
        }

        return $this;
    }

    public function removeIngredient(Ingredient $ingredient): self
    {
        if ($this->ingredients->contains($ingredient)) {
            $this->ingredients->removeElement($ingredient);
            // set the owning side to null (unless already changed)
            if ($ingredient->getRecipe() === $this) {
                $ingredient->setRecipe(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Making[]
     */
    public function getMakings(): Collection
    {
        return $this->makings;
    }

    public function addMaking(Making $making): self
    {
        if (!$this->makings->contains($making)) {
            $this->makings[] = $making;
            $making->setRecipe($this);
        }

        return $this;
    }

    public function removeMaking(Making $making): self
    {
        if ($this->makings->contains($making)) {
            $this->makings->removeElement($making);
            // set the owning side to null (unless already changed)
            if ($making->getRecipe() === $this) {
                $making->setRecipe(null);
            }
        }

        return $this;
    }


    public static function getPreparationDifficulty(){
        return [
                1 => self::DIFFICULTY_EASY_PEASY,
                2 => self::DIFFICULTY_EASY,
                3 => self::DIFFICULTY_MEDIUM,
                4 => self::DIFFICULTY_PRETTY_HARD,
                5 => self::DIFFICULTY_COMPLEX,
        ];
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->created_at;
    }

    public function setCreatedAt(?\DateTimeInterface $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updated_at;
    }

    public function setUpdatedAt(?\DateTimeInterface $updated_at): self
    {
        $this->updated_at = $updated_at;

        return $this;
    }

}
