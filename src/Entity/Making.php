<?php

namespace App\Entity;

use App\Repository\MakingRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=MakingRepository::class)
 */
class Making
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $stepmaking;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $ordermaking;

    /**
     * @ORM\ManyToOne(targetEntity=Recipe::class, inversedBy="makings")
     */
    private $recipe;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getStepmaking(): ?string
    {
        return $this->stepmaking;
    }

    public function setStepmaking(?string $stepmaking): self
    {
        $this->stepmaking = $stepmaking;

        return $this;
    }

    public function getOrdermaking(): ?int
    {
        return $this->ordermaking;
    }

    public function setOrdermaking(?int $ordermaking): self
    {
        $this->ordermaking = $ordermaking;

        return $this;
    }

    public function getRecipe(): ?Recipe
    {
        return $this->recipe;
    }

    public function setRecipe(?Recipe $recipe): self
    {
        $this->recipe = $recipe;

        return $this;
    }
}
