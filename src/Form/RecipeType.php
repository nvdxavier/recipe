<?php

namespace App\Form;

use App\Entity\Recipe;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TimeType;
use Symfony\Component\Validator\Constraints\File;

class RecipeType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title',TextType::class,['label' => 'title for ingredient'])
            ->add('makings', CollectionType::class, [
                'entry_type' => MakingType::class,
                'entry_options' => ['label' => 'Preparation Stage'],
                'allow_add' => true,
                'allow_delete' => true,
                'attr' => array('class' => 'container'),
                'by_reference' => false,
                'required' => false,
            ])

            ->add('coocking_time', TimeType::class, [
        'widget' => 'single_text',
        'placeholder' => [
            'hour' => 'Hour', 'minute' => 'Minute', 'second' => 'Second',
        ]
    ])
            ->add('preparation_time', TimeType::class, [
                'widget' => 'single_text',
                'placeholder' => [
                    'hour' => 'Hour', 'minute' => 'Minute', 'second' => 'Second',
                ]
            ])
            ->add('personal_note', TextareaType::class,['required' => false])
            ->add('difficulty', ChoiceType::class, [
                    'choices' => Recipe::getPreparationDifficulty(),
                   'expanded' => false,
                     'label' => 'Difficulty of preparation',
            ])
            ->add('recipeimage', FileType::class,
                [
                    'label' => 'Add main picture for this recipe',
                    'mapped' => false,
                    'required' => false,
                    'constraints' => [
                        new File([
                            'maxSize' => '1024k',
                            'mimeTypes' => [
                                'application/pdf',
                                'application/x-pdf',
                                 'image/jpeg',
                                 'image/jpg',
                                 'image/gif',
                                 'application/pdf',
                                 'application/x-pdf'
                            ],
                            'mimeTypesMessage' => 'Please upload a valid document',
                        ])
                    ],
                ])
            ->add('ingredients' , CollectionType::class, [
                'entry_type' => IngredientsType::class,
                'entry_options' => ['label' => false],
                'allow_add' => true,
                'allow_delete' => true,
                'attr' => array('class' => 'container'),
                'by_reference' => false,
                'required' => false,
            ])
            ->getForm();
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
//            'data_class' => Recipe::class,
        ]);
    }
}
