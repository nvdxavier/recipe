<?php

namespace App\Form;

use App\Entity\Making;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use FOS\CKEditorBundle\Form\Type\CKEditorType;

class MakingType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('stepmaking',CKEditorType::class,[
                'config' => ['uiColor' => '#ffffff'],
                'required' => true
            ])
            ->add('ordermaking',null,['required' => true])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Making::class,
        ]);
    }
}
